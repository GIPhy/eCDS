#!/bin/bash

##############################################################################################################
#                                                                                                            #
#  eCDS: extracting CDS segment(s) from contig sequence(s)                                                   #
#                                                                                                            #
   COPYRIGHT="Copyright (C) 2016-2024 Institut Pasteur"                                                      #
#                                                                                                            #
#  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU  #
#  General Public License as published by the Free Software Foundation, either version 3 of the License, or  #
#  (at your option) any later version.                                                                       #
#                                                                                                            #
#  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even  #
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public  #
#  License for more details.                                                                                 #
#                                                                                                            #
#  You should have received a copy of the  GNU General Public License along with this program.  If not, see  #
#  <http://www.gnu.org/licenses/>.                                                                           #
#                                                                                                            #
#  Contact:                                                                                                  #
#   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr  #
#   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr  #
#   Centre de Ressources Biologiques de l'Institut Pasteur (CRBIP)             research.pasteur.fr/en/b/VTq  #
#   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr  #
#                                                                                                            #
#            4888888883                                                                                      #
#         48800007   4003 1                                                                                  #
#      4880000007   400001 83        101  100    01   4000009  888888888 101 888888888 08    80 888888888    #
#     4000000008    8000001 83       181  10101  01  601     1    181    181    181    08    80    181       #
#    40000000008    8000001 803      181  10 101 01    60003      181    181    181    08    80    181       #
#   100888880008    800007 60003     181  10  10101  4     109    181    181    181    68    87    181       #
#   81     68888    80887 600008     101  10    001   0000007     101    101    101     600009     101       #
#   808883     1    887  6000008                                                                             #
#   8000000003         480000008                                                                             #
#   600000000083    888000000007     10000000     40      4000009  888888888 10000000  08    80  1000000     #
#    60000000008    80000000007      180    39   4000    601     1    181    10        08    80  10    39    #
#     6000000008    8000000007       18000007   47  00     60003      181    1000000   08    80  1000007     #
#      680000008    800000087        180       40000000  4     109    181    10        68    87  10   06     #
#        6888008    8000887          100      47      00  0000007     101    10000000   600009   10    00    #
#            688    8887                                                                                     #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ============                                                                                               #
# = VERSIONS =                                                                                               #
# ============                                                                                               #
#                                                                                                            #
  VERSION=1.2-240328ac;                                                                                      #
# + deals with NCBI BLAST+ version >= 2.15.0                                                                 #
# + modified default value for option -b (from 0 to 50)                                                      #
# + updated code style                                                                                       #
#                                                                                                            #
# VERSION=1.1.220408ac;                                                                                      #
# + now using NCBI BLAST+ (version >= 2.12.0)                                                                #
# + creates blastdb in a dedicated temp file (required for parallelized runs)                                #
#                                                                                                            #
# VERSION=1.0.210724ac;                                                                                      #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# =============                                                                                              #
# = CONSTANTS =                                                                                              #
# =============                                                                                              #
#                                                                                                            #
# -- PWD: directory containing the current script ---------------------------------------------------------  #
#                                                                                                            #
  PWD="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)";
#                                                                                                            # 
# -- n/a --------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  NA="._N.o.N._.A.p.P.l.I.c.A.b.L.e_.";
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ================                                                                                           #
# = REQUIREMENTS =                                                                                           #
# ================                                                                                           #
#                                                                                                            #
# -- gawk version >= 4 ------------------------------------------------------------------------------------  #
#                                                                                                            #
  GAWK_BIN=gawk;
  if [ ! $(command -v $GAWK_BIN) ]; then echo "$GAWK_BIN not found"                       >&2 ; exit 1 ; fi
  BAWK="$GAWK_BIN";
  CAWK="$GAWK_BIN -F,";
  TAWK="$GAWK_BIN -F\\t";
#                                                                                                            #
# -- NCBI BLAST+ (version >= 2.12.0) ----------------------------------------------------------------------  #
#                                                                                                            #
  export BLAST_USAGE_REPORT=false;                       ## from https://www.ncbi.nlm.nih.gov/books/NBK569851/ 
#                                                                                                            #
# -- makeblastdb                                                                                             #
#                                                                                                            #
  MAKEBLASTDB_BIN=makeblastdb;
  if [ ! $(command -v $MAKEBLASTDB_BIN) ]; then echo "$MAKEBLASTDB_BIN not found"         >&2 ; exit 1 ; fi
  version="$($MAKEBLASTDB_BIN -version | $BAWK '(NR==2){print$3}' | $BAWK -F"." '($1>=2&&$2>=12)')";
  if [ -z "$version" ]; then echo "incorrect version: $MAKEBLASTDB_BIN"                   >&2 ; exit 1 ; fi
  MAKEBLASTDB_STATIC_OPTIONS="-dbtype nucl";
  MAKEBLASTDB="$MAKEBLASTDB_BIN $MAKEBLASTDB_STATIC_OPTIONS";
#                                                                                                            #
# -- tblastn                                                                                                 #
#                                                                                                            #
  TBLASTN_BIN=tblastn;
  if [ ! $(command -v $TBLASTN_BIN) ]; then echo "$TBLASTN_BIN not found"                 >&2 ; exit 1 ; fi
  version="$($TBLASTN_BIN -version | $BAWK '(NR==2){print$3}' | $BAWK -F"." '($1>=2&&$2>=12)')";
  if [ -z "$version" ]; then echo "incorrect version: $TBLASTN_BIN"                       >&2 ; exit 1 ; fi
  TBLASTN_STATIC_OPTIONS="-task tblastn -evalue 10 -seg no -word_size 2";
  TBLASTN_STATIC_OPTIONS="$TBLASTN_STATIC_OPTIONS -max_intron_length 1000 -xdrop_gap_final 1000"; 
  TBLASTN_STATIC_OPTIONS="$TBLASTN_STATIC_OPTIONS -num_threads 2 -mt_mode 2"; 
  TBLASTN="$TBLASTN_BIN $TBLASTN_STATIC_OPTIONS";
#                                                                                                            #
# -- eFASTA -----------------------------------------------------------------------------------------------  #
#                                                                                                            #
  EFASTA_BIN=eFASTA;
  if [ ! $(command -v $EFASTA_BIN) ]; then echo "$EFASTA_BIN not found"                   >&2 ; exit 1 ; fi
  EFASTA_STATIC_OPTIONS="-fcds";
  EFASTA="$EFASTA_BIN $EFASTA_STATIC_OPTIONS";
#                                                                                                            #
##############################################################################################################


##############################################################################################################
#                                                                                                            #
# ============                                                                                               #
# = DOC      =                                                                                               #
# ============                                                                                               #
#                                                                                                            #
mandoc() {
  echo -e "\n\033[1m eCDS v$VERSION                            $COPYRIGHT\033[0m";
  cat <<EOF

 https://gitlab.pasteur.fr/GIPhy/eCDS

 USAGE : eCDS   -q <infile>     -s <infile>   -o <basename> 
               [-p <ppos>]     [-c <qcov>]   [-b <bitscore>]
               [-S <integer>]  [-f]   [-a]   [-h]

 where options are :

   -q <infile>    query file containing either a sequence (FASTA format) or a position-
                  specific scoring matrix (PSSM; ASN.1 format)
   -s <infile>    subject sequence file (FASTA format)
   -o <basename>  to write the inferred  CDS into the output  file names [basename].fna 
                  (codon) and [basename].faa (translated amino acids)                  
   -p <integer>   minimum allowed  percentage of  positive-scoring  amino acid  matches 
                  (default: 50)
   -c <integer>   minimum allowed percentage of query coverage (default: 70)
   -b <integer>   minimum allowed bit score value (default: 50)
   -f             writes only CDS inferred from the first best hit (default: not set)
   -S <integer>   to accept alternate START codons when searching for the complete CDS:
                    1 = ATG (default)
                    2 = ATG GTG TTG
                    3 = ATG GTG TTG CTG ATT
                    4 = ATG GTG TTG CTG ATT ATC ATA
   -a             appends CDS to  the end of  the FASTA output  files when they already 
                  exist (default: content replaced)
   -h             prints this help and exits
 
EOF
} 
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# =============                                                                                              #
# = FUNCTIONS =                                                                                              #
# =============                                                                                              #
#                                                                                                            #
# -- randfile ---------------------------------------------------------------------------------------------  #
# >> creates and returns a random file name that does not exist from the specified basename $1               #
#                                                                                                            #
randfile() {
  local rdf="$(mktemp $1.XXXXXXXXX)";
  echo $rdf ;
}
#                                                                                                            #
##############################################################################################################


##############################################################################################################
#                                                                                                            #
# READING OPTIONS                                                                                            #
#                                                                                                            #
##############################################################################################################

if [ $# -lt 1 ]; then mandoc ; exit 1 ; fi

QUERY="$NA";
SUBJECT="$NA";
BASEFILE="$NA";
MINPPOS=50;
MINQCOV=70;
MINBITS=50;
CSTART=1;
APPEND=false;
ONLYFIRST=false;

while getopts :q:s:o:p:c:b:S:afh option
do
  case $option in
  q)  QUERY=$OPTARG     ;;
  s)  SUBJECT=$OPTARG   ;;
  o)  BASEFILE=$OPTARG  ;;
  p)  MINPPOS=$OPTARG   ;;
  c)  MINQCOV=$OPTARG   ;;
  b)  MINBITS=$OPTARG   ;;
  S)  CSTART=$OPTARG    ;;
  a)  APPEND=true       ;;
  f)  ONLYFIRST=true    ;;
  h)  mandoc ;  exit 0  ;;
  \?) mandoc ;  exit 1  ;;
  esac
done

if [ "$QUERY" == "$NA" ];        then echo "no query file (option -q)"                            >&2 ; exit 1 ; fi
if [ "$SUBJECT" == "$NA" ];      then echo "no subject file (option -s)"                          >&2 ; exit 1 ; fi
if [ "$BASEFILE" == "$NA" ];     then echo "no basename specified (option -o)"                    >&2 ; exit 1 ; fi
if [ ! -s $QUERY ];              then echo "no or empty query file (option -q): $QUERY"           >&2 ; exit 1 ; fi
if [ ! -s $SUBJECT ];            then echo "no or empty subject file (option -s): $INFILE"        >&2 ; exit 1 ; fi
if ! [[ $MINPPOS =~ ^[0-9]+$ ]]; then echo "incorrect integer value (option -p): $MINPPOS"        >&2 ; exit 1 ; fi
if [ $MINPPOS -gt 100 ];         then echo "integer percentage too high (option -p): $MINPPOS"    >&2 ; exit 1 ; fi 
if ! [[ $MINQCOV =~ ^[0-9]+$ ]]; then echo "incorrect integer value (option -c): $MINQCOV"        >&2 ; exit 1 ; fi
if [ $MINQCOV -gt 100 ];         then echo "integer percentage too high (option -c): $MINQCOV"    >&2 ; exit 1 ; fi
if ! [[ $MINBITS =~ ^[0-9]+$ ]]; then echo "incorrect integer value (option -b): $MINBITS"        >&2 ; exit 1 ; fi

[ "$CSTART" == "2" ] && EFASTA="$EFASTA -start PROK";
[ "$CSTART" == "3" ] && EFASTA="$EFASTA -start PROK+";
[ "$CSTART" == "4" ] && EFASTA="$EFASTA -start PROK++";


##############################################################################################################
#                                                                                                            #
# INIT                                                                                                       #
#                                                                                                            #
##############################################################################################################

export LC_ALL=C ;

TBNOUT=$BASEFILE.tbn ;
LOG=$BASEFILE.log.txt ;
DBSBJT=$(randfile $SUBJECT);
TMP=$(randfile $BASEFILE);
QRY=$(randfile $QUERY);

finalizer() {
  rm -f $TBNOUT $TMP $QRY $DBSBJT $DBSBJT.{ndb,nhr,nin,njs,not,nsq,ntf,nto} &>/dev/null ;
}
trap "echo -n interrupting ; echo -n . ; kill -9 $(jobs -pr) &> /dev/null ; echo -n . ; finalizer ; echo -n . ; wait ; exit 1 "  SIGTERM SIGINT SIGQUIT SIGHUP TERM INT QUIT HUP ;

if ! $APPEND
then
  [ -e $BASEFILE.faa ] && rm -f $BASEFILE.faa ;
  [ -e $BASEFILE.fna ] && rm -f $BASEFILE.fna ;
fi


##############################################################################################################
#                                                                                                            #
# FORMATTING SUBJECT                                                                                         #
#                                                                                                            #
##############################################################################################################

echo -n -e "SUBJECT: $(basename $SUBJECT)\t\t";

$MAKEBLASTDB -in $SUBJECT -out $DBSBJT > /dev/null ;


##############################################################################################################
#                                                                                                            #
# TBLASTN SEARCH                                                                                             #
#                                                                                                            #
##############################################################################################################

echo -n -e "QUERY: $(basename $QUERY)\t\t";

if [ $(grep -c -m1 "^>" $QUERY) -eq 0 ]
then
  optin="-in_pssm";
  # rescaling PSSM
  sf=$(grep "scalingFactor" $QUERY | $BAWK '{print$2}' | tr -d ',');
  [ -z "$sf" ] && sf=1;
  $BAWK -v sf=$sf '($1=="scores")       {rescale=1;
                                         print;
                                         next;
                                        }
                   (rescale&&$1=="},")  {rescale=0}
                   (rescale)            {s=$1;
                                         n=sub(",","",s);
                                         s=sprintf("%d",(s/sf));
                                         printf"        "s;
                                         print (n==0)?"":",";
                                         next;
                                        }
                   ($1=="scalingFactor"){print"      scalingFactor 1,";
                                         next;
                                        }
                                        {print}' $QUERY > $QRY ;
else
  optin="-query";
  # getting the first sequence
  $BAWK '!/^>/{s=s$0;next}(s!=""){print s;s=""}{print}END{print s}' $QUERY | head -2 > $QRY ;
fi

echo -e "#query\tsubject\tsstart\tsend\tpident\tppos\tqcov\tevalue\tbitscore" > $TBNOUT ;
#                                fields=    1       2    3      4    5      6    7      8    9      10
$TBLASTN $optin $QRY -db $DBSBJT -outfmt "6 saccver qlen qstart qend sstart send pident ppos evalue bitscore" |
  $TAWK -v q=$(basename ${QUERY%.*}) -v p=$MINPPOS -v c=$MINQCOV -v b=$MINBITS '($10>=b&&$8>=p&&(qcov=(100*($4-$3+1)/$2))>=c){print q"\t"$1"\t"$5"\t"$6"\t"$7"\t"$8"\t"qcov"\t"$9"\t"$10}' >> $TBNOUT ;

rm -f $DBSBJT $DBSBJT.{ndb,nhr,nin,njs,not,nsq,ntf,nto} $QRY ;


##############################################################################################################
#                                                                                                            #
# EXTRACTING CDS                                                                                             #
#                                                                                                            #
##############################################################################################################

n=$(grep -v "^#query" $TBNOUT | wc -l);
if [ $n -eq 0 ]
then
  echo "no hit found" ;
  rm -f $TBNOUT $TMP ;
  exit ;
fi
if [ ! -s $LOG ]
then
  cat $TBNOUT >> $LOG ;
else
  grep -v "^#query" $TBNOUT >> $LOG ;
fi
if [ $n -eq 1 ]
then
  echo -n "one hit found" ;
else
  echo -n "$n hits found" ;
fi

while read -r _ id s e _ _ _ _ _
do
  if [ "$s" == "sstart" ]; then continue ; fi
  $EFASTA -fasta $SUBJECT -coord $id:$s-$e -outname $TMP ;
  cat $TMP.faa >> $BASEFILE.faa ;
  cat $TMP.fna >> $BASEFILE.fna ;
  rm -f $TMP.faa $TMP.fna ;
  if $ONLYFIRST ; then break ; fi
done < $TBNOUT

if [ $n -eq 1 ]
then
  lgt=$(sed -n '2p' $BASEFILE.faa | tr -d '\n' | wc -c);
  echo -n " ($lgt aa)" ;
fi  
echo ": $(basename $BASEFILE).faa $(basename $BASEFILE).fna" ;


##############################################################################################################
#                                                                                                            #
# EXIT                                                                                                       #
#                                                                                                            #
##############################################################################################################

rm -f $TBNOUT $TMP ;

exit ;


