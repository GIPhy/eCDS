# eCDS

_eCDS_ (extracting coding sequences) is a command line tool written in [Bash](https://www.gnu.org/software/bash/) to ease the extraction of coding sequence(s) (CDS; without intron) from a nucleotide sequence FASTA file (typically, a set of contig sequences). Given a specified query file (containing either an amino acid sequence or a position-specific scoring matrix), _eCDS_ first runs a TBLASTN similarity search (e.g. Gertz et al. 2006) against the specified subject sequence(s), and next extracts the CDS corresponding to each selected BLAST hit.

_eCDS_ runs on UNIX, Linux and most OS X operating systems.




## Dependencies

You will need to install the required programs and tools listed in the following table, or to verify that they are already installed with the required version.

<div align="center">

| program                                                                             | package                                                          | version    | sources                                                                                                   |
|:----------------------------------------------------------------------------------- |:----------------------------------------------------------------:| ----------:|:--------------------------------------------------------------------------------------------------------- |
| [_eFASTA_](https://gitlab.pasteur.fr/GIPhy/eFASTA)                                  | -                                                                | &ge; 1.2b  | [gitlab.pasteur.fr/GIPhy/eFASTA](https://gitlab.pasteur.fr/GIPhy/eFASTA)                                                |
| [_gawk_](https://www.gnu.org/software/gawk/)                                        | -                                                                | > 4.0.0    | [ftp.gnu.org/gnu/gawk](http://ftp.gnu.org/gnu/gawk/)                                                      |
| _makeblastdb_ <br> _tblastn_                                                        | [blast+](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-10-421)               | &ge; 2.15.0 | [ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST](ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST)                                |

</div>

## Installation and execution

**A.** Clone this repository with the following command line:

```bash
git clone https://gitlab.pasteur.fr/GIPhy/eCDS.git
```

**B.** Give the execute permission to the file `eCDS.sh`:

```bash
chmod +x eCDS.sh
```

**C.** Execute _eCDS_ with the following command line model:

```bash
./eCDS.sh  [options]
```

**D.** If at least one of the required program (see [Dependencies](#dependencies)) is not available on your `$PATH` variable (or if one compiled binary has a different default name), _eCDS_ will exit with an error message.
When running _eCDS_ without option, a documentation should be displayed; otherwise, the name of the missing program is displayed before existing.
In such a case, edit the file `eCDS.sh` and indicate the local path to the corresponding binary(ies) within the code block `REQUIREMENTS` (approximately lines 80-120).
For each required program, the table below reports the corresponding variable assignment instruction to edit (if needed) within the code block `REQUIREMENTS`

<div align="center">
<sup>

| program          | variable assignment              |   | program          | variable assignment              |
|:---------------- |:-------------------------------- | - |:---------------- |:-------------------------------- |
| _eFASTA_         | `EFASTA_BIN=eFASTA;`             |   | _makeblastdb_    | `MAKEBLASTDB_BIN=makeblastdb;`   |
| _gawk_           | `GAWK_BIN=gawk;`                 |   | _tblastn_        | `TBLASTN_BIN=tblastn;`           |

</sup>
</div>

Note that depending on the installation of some required programs, the corresponding variable can be assigned with complex commands. 
For example, as _eFASTA_ is a Java tool that can be run using a Java virtual machine, the executable jar file `EFASTA.jar` can be used by _eCDS_ by editing the corresponding variable assignment instruction as follows: `EFASTA_BIN="java -jar EFASTA.jar"`.


## Usage

Run _eCDS_ without option to read the following documentation:

```
 USAGE : eCDS   -q <infile>    -s <infile>   -o <basename>
               [-p <ppos>]    [-c <qcov>]   [-b <bitscore>]
               [-S <integer>] [-f]   [-a]   [-h]

 where options are :

   -q <infile>    query file containing either a sequence (FASTA format) or a position-
                  specific scoring matrix (PSSM; ASN.1 format)
   -s <infile>    subject sequence file (FASTA format)
   -o <basename>  to write the inferred  CDS into the output  file names [basename].fna
                  (codon) and [basename].faa (translated amino acids)
   -p <integer>   minimum allowed  percentage of  positive-scoring  amino acid  matches
                  (default: 50)
   -c <integer>   minimum allowed percentage of query coverage (default: 70)
   -b <integer>   minimum allowed bit score value (default: 50)
   -f             writes only CDS inferred from the first best hit (default: not set)
   -S <integer>   to accept alternate START codons when searching for the complete CDS:
                    1 = ATG (default)
                    2 = ATG GTG TTG
                    3 = ATG GTG TTG CTG ATT
                    4 = ATG GTG TTG CTG ATT ATC ATA
   -a             appends CDS to  the end of  the FASTA output  files when they already
                  exist (default: content replaced)
   -h             prints this help and exits
```

## Notes

* _eCDS_ is able to consider as query (option `-q`) either a single amino acid sequence in FASTA format or a position-specific scoring matrix (PSSM) in ASN.1 format (see below). When using a FASTA file as query, only the first sequence is considered. When using a PSSM file as query, rescaling is automatically performed to be read by the _tblastn_ binary (i.e. scalingFactor = 1 into the ASN.1-formatted PSSM file).

* A PSSM file summarizes a multiple amino acid sequence alignment. It is therefore very useful to use a PSSM as a generic query against genome sequences belonging to any genera (e.g. Altschul et al. 1997).<br> A very large collection of PSSM can be found at [ftp.ncbi.nih.gov/pub/mmdb/cdd/](https://ftp.ncbi.nih.gov/pub/mmdb/cdd/), into the (large) archive [_cdd.tar.gz_](https://ftp.ncbi.nih.gov/pub/mmdb/cdd/cdd.tar.gz). A selection of PSSM for highly conserved genes can also be found within the [PhyloM](https://giphy.pasteur.fr/PhyloM/) repository.<br> For a given gene name (e.g. "ribosomal protein L3"), the associated conserved domain and/or protein cluster PSSM identifier(s) can be easily retrieved using the search tool at [www.ncbi.nlm.nih.gov/cdd](https://www.ncbi.nlm.nih.gov/cdd/) or [www.ncbi.nlm.nih.gov/proteinclusters](https://www.ncbi.nlm.nih.gov/proteinclusters); complete lists are also available at [ftp.ncbi.nih.gov/genomes/CLUSTERS/](https://ftp.ncbi.nih.gov/genomes/CLUSTERS/) (files _\*\_clusters.txt_).<br> For a given amino acid CDS with GenBank identifier (e.g. NP_417779), the associated conserved domain and/or protein cluster PSSM identifier(s) can be retrieved using the following URL: &nbsp; [www.ncbi.nlm.nih.gov/Structure/cdd/wrpsb.cgi?FULL&SEQUENCE=NP_417779](https://www.ncbi.nlm.nih.gov/Structure/cdd/wrpsb.cgi?FULL&SEQUENCE=NP_417779) &nbsp; (just change the accession identifier at the end of the URL).<br> Note that using PSSM generated from the automatically aligned sequences classified as stable clusters in the [NCBI Protein Clusters database](https://www.ncbi.nlm.nih.gov/proteinclusters) (i.e. PRK data) enables to observe accurate results when using _eCDS_.

* After carrying out a TBLASTN search of the specified query (option `-q`) against the specified subject FASTA file (option `-s`), non-significant BLAST hits are filtered out according to three possible criteria: <br> &nbsp; &bull; minimum percentage of positive-scoring amino acid matches (option `-p`; default: _ppos_ = 50%), <br> &nbsp; &bull; minimum percentage of query coverage (option `-c`; default: _qcov_ = 70%), <br> &nbsp; &bull; minimum bit score value (option `-b`; default: _bitscore_ = 50, as suggested in Pearson 2013). <br> The first criterion cutoff can be enlarged when using a query sequence that is expected to be closely related to the subject sequence (e.g. `-p 80`), as well as the second one. The third criterion can be useful when using one of the query PSSM distributed by the NCBI, as a minimum bitscore threshold is available for each of them. For example, PRK05644 (see example below), a PSSM associated to the gene _gyrB_, can be used as query with the bitscore threshold specified at the corresponding webpage: [www.ncbi.nlm.nih.gov/Structure/cdd/cddsrv.cgi?uid=PRK05644](https://www.ncbi.nlm.nih.gov/Structure/cdd/cddsrv.cgi?uid=PRK05644) ('+ Statistics' on the left); note also that updated lists of bitscore thresholds are available at [ftp.ncbi.nih.gov/pub/mmdb/cdd/](https://ftp.ncbi.nih.gov/pub/mmdb/cdd/) (files _bitscore\_specific\_\*.txt_). However, a simple approach is also to run _eCDS_ with relaxed criteria and only consider the first best BLAST hit (option `-f`).

* Finally, for each selected BLAST hit, the corresponding subject sequence region is considered to extract the smallest putative CDS containing that region, using [_eFASTA_](https://gitlab.pasteur.fr/GIPhy/eFASTA). Note that the determination of the CDS can be tuned by accepting alternate START codons (option `-S`).

* Amino acid and codon sequences are written into FASTA files with extensions .faa and .fna, respectively. The base name of the output files should be specified using option `-o`. Note that a tab-delimited log file containing several BLAST results (i.e. query file name, subject sequence name,	start and end indices within the subject sequence, identity and positive-scoring match percentages, query coverage percentage, TBLASTN E-value and bitscore) is also written (extension .log.txt).



## Example

In order to illustrate the usefulness of _eCDS_, the directory _example/_ contains the whole chromosome sequence of _Bacillus subtilis_ strain 168<sup>T</sup> (GenBank accession [AL009126.3](https://www.ncbi.nlm.nih.gov/nuccore/AL009126.3/)) in FASTA format, as well as four PSSM files:
<br> &nbsp; &nbsp; &bull; _PRK00013.groEL.smp_ corresponding to the protein cluster [PRK00013](https://www.ncbi.nlm.nih.gov/Structure/cdd/cddsrv.cgi?uid=PRK00013) associated to the gene _groEL_,
<br> &nbsp; &nbsp; &bull; _PRK00380 .panC.smp_ corresponding to the protein cluster [PRK00380](https://www.ncbi.nlm.nih.gov/Structure/cdd/cddsrv.cgi?uid=PRK00380) associated to the gene _panC_,
<br> &nbsp; &nbsp; &bull; _PRK00405.rpoB.smp_ corresponding to the protein cluster [PRK00405](https://www.ncbi.nlm.nih.gov/Structure/cdd/cddsrv.cgi?uid=PRK00405) associated to the gene _rpoB_,
<br> &nbsp; &nbsp; &bull; _PRK05644.gyrB.smp_ corresponding to the protein cluster [PRK05644](https://www.ncbi.nlm.nih.gov/Structure/cdd/cddsrv.cgi?uid=PRK05644) associated to the gene _gyrB_.


Basically, the four following command lines can be each used to extract the four _groEL_, _panC_, _rpoB_ and _gyrB_ CDS, respectively, from the _B. subtilis_ 168<sup>T</sup> chromosome:
```bash
eCDS.sh -q PRK00013.groEL.smp -s Bacillus.subtilis.168.fasta  -o bc.groEL
eCDS.sh -q PRK00380.panC.smp  -s Bacillus.subtilis.168.fasta  -o bc.panC
eCDS.sh -q PRK00405.rpoB.smp  -s Bacillus.subtilis.168.fasta  -o bc.rpoB
eCDS.sh -q PRK05644.gyrB.smp  -s Bacillus.subtilis.168.fasta  -o bc.gyrB
```

For _groEL_ gene, the output file _bc.groEL.faa_ (see _example/_) contains the expected amino acid sequence, i.e. identical to the annotated _groEL_ gene within the _B. subtilis_ 168<sup>T</sup> chromosome [AL009126.3](https://www.ncbi.nlm.nih.gov/nuccore/AL009126.3/) (i.e. [CAB12422](https://www.ncbi.nlm.nih.gov/protein/2632916)). For _panC_ gene, _eCDS_ also extracts an amino acid sequence (_bc.panC.faa_ in _example/_) that is identical to the annotated one (i.e. [CAB14158](https://www.ncbi.nlm.nih.gov/protein/2634660)).

For _rpoB_ gene, one can observe that the above command line leads to an amino acid sequence that is not starting with a START codon:

```
>AL009126.3::121907-125497
GVNQLTGQLVQYGRHRQRRSYARISEVLELPNLIEIQTSSYQWFLDEGLREMFQDISPIEDFTGNLSLEFIDYSLGEPKYPVEESKERDVTYSAPLRVKVRLINKETGEVKDQDVFMGDFPIMTDTGTFIINGAERVIVSQLVRSPSVYFSGKVDKNGKKGFTATVIPNRGAWLEYETDAKDVVYVRIDRTRKLPVTVLLRALGFGSDQEILDLIGENEYLRNTLDKDNTENSDKALLEIYERLRPGEPPTVENAKSLLDSRFFDPKRYDLANVGRYKINKKLHIKNRLFNQRLAETLVDPETGEILAEKGQILDRRTLDKVLPYLENGIGFRKLYPNGGVVEDEVTLQSIKIFAPTDQEGEQVINVIGNAYIEEEIKNITPADIISSISYFFNLLHGVGDTDDIDHLGNRRLRSVGELLQNQFRIGLSRMERVVRERMSIQDTNTITPQQLINIRPVIASIKEFFGSSQLSQFMDQTNPLAELTHKRRLSALGPGGLTRERAGMEVRDVHYSHYGRMCPIETPEGPNIGLINSLSSYAKVNRFGFIETPYRRVDPETGKVTGRIDYLTADEEDNYVVAQANARLDDEGAFIDDSIVARFRGENTVVSRNRVDYMDVSPKQVVSAATACIPFLENDDSNRALMGANMQRQAVPLMQPEAPFVGTGMEYVSGKDSGAAVICKHPGIVERVEAKNVWVRRYEEVDGQKVKGNLDKYSLLKFVRSNQGTCYNQRPIVSVGDEVVKGEILADGPSMELGELALGRNVMVGFMTWDGYNYEDAIIMSERLVKDDVYTSIHIEEYESEARDTKLGPEEITRDIPNVGEDALRNLDDRGIIRIGAEVKDGDLLVGKVTPKGVTELTAEERLLHAIFGEKAREVRDTSLRVPHGGGGIIHDVKVFNREDGDELPPGVNQLVRVYIVQKRKISEGDKMAGRHGNKGVISKILPEEDMPYLPDGTPIDIMLNPLGVPSRMNIGQVLELHMGMAARYLGIHIASPVFDGAREEDVWETLEEAGMSRDAKTVLYDGRTGEPFDNRVSVGIMYMIKLAHMVDDKLHARSTGPYSLVTQQPLGGKAQFGGQRFGEMEVWALEAYGAAYTLQEILTVKSDDVVGRVKTYEAIVKGDNVPEPGVPESFKVLIKELQSLGMDVKILSGDEEEIEMRDLEDEEDAKQADGLALSGDEEPEETASADVERDVVTKE
```

However, such a problem can be generally fixed by using alternate START codons (option `-S`). This can be achieved by the following example command line:

```bash
eCDS.sh -q PRK00405.rpoB.smp  -s Bacillus.subtilis.168.fasta  -S 2  -o bc.rpoB
```

that writes the output file _bc.rpoB.faa_ (see _example/_) containing an amino acid sequence that is identical to the annotated _B. subtilis_ 168<sup>T</sup> _rpoB_ CDS (i.e. [CAB11883](https://www.ncbi.nlm.nih.gov/protein/225184649)).

Finally, for _gyrB_ gene, one can observe that the above command line leads to two extracted sequences. As expected, the first one (i.e. first best BLAST hit) is identical to the _B. subtilis_ 168<sup>T</sup> _gyrB_ CDS (i.e. [CAB11782](https://www.ncbi.nlm.nih.gov/protein/2632273)). However, the second sequence corresponds to the gene _parE_ (subunit B of DNA topoisomerase IV), a well-known paralog of _gyrB_ (e.g. Poirier et al. 2018). A way to circumvent this problem is to only consider the first best BLAST hit (option `-f`). However, an alternative way is to use the bitscore threshold (option `-b`) suggested for PRK05644 (available at [www.ncbi.nlm.nih.gov/Structure/cdd/cddsrv.cgi?uid=PRK05644](https://www.ncbi.nlm.nih.gov/Structure/cdd/cddsrv.cgi?uid=PRK05644)); this leads to the following example command line:

```bash
eCDS.sh -q PRK05644.gyrB.smp  -s Bacillus.subtilis.168.fasta  -p 0  -c 0  -b 1029  -o bc.gyrB
```

that writes only the _gyrB_ amino acid and codon sequences into the files _bc.gyrB.faa_ and _bc.gyrB.fna_, respectively (see _example/_).






## References



Altschul SF, Madden TL, Schäffer AA, Zhang J, Zhang Z, Miller W, Lipman DJ (1997) _Gapped BLAST and PSI-BLAST: a new generation of protein database search programs_. **Nucleic Acids Research**, 25(17):2289-3402. [doi:10.1093/nar/25.17.3389](https://doi.org/10.1093/nar/25.17.3389).

Gertz EM, Yu YK, Agarwala R, Schäffer AA, Altschul SF (2006) _Composition-based statistics and translated nucleotide searches: Improving the TBLASTN module of BLAST_. **BMC Biology**, 4:41. [doi:10.1186/1741-7007-4-41](https://doi.org/10.1186/1741-7007-4-41).

Pearson WR (2013) _An Introduction to Sequence Similarity (“Homology”) Searching_. **Current Protocols in Bioinformatics**, 42:3.1.1-3.1.8. [doi:10.1002/0471250953.bi0301s42](https://doi.org/10.1002/0471250953.bi0301s42).

Poirier S, Rué O, Peguilhan R, Coeuret G, Zagorec M, Champomier-Vergès M-C, Loux V, Chaillou S (2018) _Deciphering intra-species bacterial diversity of meat and seafood spoilage microbiota using gyrB amplicon sequencing: A comparative analysis with 16S rDNA V3-V4 amplicon sequencing_. **PLoS One**, 13(9):e0204629. [doi:10.1371/journal.pone.0204629](https://doi.org/10.1371/journal.pone.0204629).



